import 'package:ds_app/animation.dart';
import 'package:ds_app/home.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'model/items.dart';
import 'dart:convert';

void main() {
  runApp(Mainpage());
}

// ignore: camel_case_types
class Mainpage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Deepsense",
      theme: ThemeData(
        primarySwatch: Colors.teal,
      ),
      home: new Mypage(),
    );
  }
}

class Mypage extends StatefulWidget {
  @override
  _MypageState createState() => _MypageState();
}

class _MypageState extends State<Mypage> {
  Future getData() async {
    var res = await http.get("https://jsonplaceholder.typicode.com/posts");
    print(res.body);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('images/main_background.JPG'),
                  fit: BoxFit.cover)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center, // To center the column
            children: [
              Row(
                mainAxisAlignment:
                    MainAxisAlignment.center, // TO center the row
                children: [
                  Container(
                      child: Image.asset(
                    'images/logo.png',
                    height: 100.0,
                    color: Colors.white,
                  )), // image throw assest
                  Text(
                    'deep',
                    style: TextStyle(fontSize: 50, color: Colors.black),
                  ),
                  Text(
                    'sense',
                    style: TextStyle(fontSize: 50, color: Colors.white),
                  ),
                ],
              ),
              SizedBox(
                // For margin from above row
                height: 50.0,
              ),
              Row(
                mainAxisAlignment:
                    MainAxisAlignment.center, // TO center the row
                children: [
                  ButtonTheme(
                    // Button modification
                    buttonColor: Colors.white,
                    height: 50.0,
                    minWidth: 150.0,
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => home(),
                          ),
                        );
                      },
                      child: Text(
                        "GO",
                        style: TextStyle(
                            color: Colors.teal,
                            fontSize: 24.0,
                            fontWeight:
                                FontWeight.bold), // Button text modification
                      ),
                      shape: RoundedRectangleBorder(
                        // button radius
                        borderRadius: BorderRadius.circular(90.0),
                      ),
                    ),
                  ),
                  ButtonTheme(
                    // Button modification
                    buttonColor: Colors.white,
                    height: 50.0,
                    minWidth: 150.0,
                    child: RaisedButton(
                      onPressed: () {
                        getData();
                      },
                      child: Text(
                        "Http",
                        style: TextStyle(
                            color: Colors.teal,
                            fontSize: 24.0,
                            fontWeight:
                                FontWeight.bold), // Button text modification
                      ),
                      shape: RoundedRectangleBorder(
                        // button radius
                        borderRadius: BorderRadius.circular(90.0),
                      ),
                    ),
                  ),

                  //  ButtonTheme(
                  //   // Button modification
                  //   buttonColor: Colors.white,
                  //   height: 50.0,
                  //   minWidth: 150.0,
                  //   child: RaisedButton(
                  //     onPressed: () {
                  //       Navigator.of(context).push(
                  //         MaterialPageRoute(
                  //           builder: (context) => HomePage(),
                  //         ),
                  //       );
                  //     },
                  //     child: Text("GO",
                  //       style: TextStyle(
                  //           color: Colors.teal,
                  //           fontSize: 24.0,
                  //           fontWeight:
                  //               FontWeight.bold), // Button text modification
                  //     ),
                  //     shape: RoundedRectangleBorder(
                  //       // button radius
                  //       borderRadius: BorderRadius.circular(90.0),
                  //     ),
                  //   ),
                  // ),
                ],
              ),
              SizedBox(
                height: 50.0,
              ),
              ButtonTheme(
                // Button modification
                buttonColor: Colors.white,
                height: 50.0,
                minWidth: 150.0,
                child: RaisedButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        PageRouteBuilder(
                            transitionDuration: Duration(seconds: 1),
                            transitionsBuilder:
                                (context, animation, animationTime, child) {
                              return ScaleTransition(
                                alignment: Alignment.bottomCenter,
                                scale: animation,
                                child: child,
                              );
                            },
                            pageBuilder: (context, animation, animationTime) {
                              return AnimatedContainerApp();
                            }));
                  },
                  child: Text(
                    "Animation",
                    style: TextStyle(
                        color: Colors.teal,
                        fontSize: 24.0,
                        fontWeight:
                            FontWeight.bold), // Button text modification
                  ),
                  shape: RoundedRectangleBorder(
                    // button radius
                    borderRadius: BorderRadius.circular(90.0),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),

      //
    );
  }
}
