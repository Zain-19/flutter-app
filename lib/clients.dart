import 'package:ds_app/bottom.dart';
import 'package:ds_app/drawer.dart';
import 'package:flutter/material.dart';
import 'services.dart';

class Client_grid extends StatefulWidget {
  @override
  _Client_gridState createState() => _Client_gridState();
}

class _Client_gridState extends State<Client_grid> {
  var ser = Services();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Mydrawer(),
      appBar: AppBar(
        backgroundColor: Colors.teal,
        title: Text(
          'Our Clients',
          style: TextStyle(
              fontFamily: 'Chilanka',
              fontSize: 24.0,
              fontWeight: FontWeight.bold,
              color: Colors.white),
        ),
        centerTitle: true,
      ),
      body: GridView.count(
        crossAxisCount: 2,
        padding: EdgeInsets.all(5.0),
        mainAxisSpacing: 5.0,
        crossAxisSpacing: 5.0,
        children: [
          Container(
            child: FlatButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => Services(),
                ));
              },
              color: Colors.teal[100],
              child: Image.asset('images/bayer.jpg'),
            ),
          ),
          Container(
            child: FlatButton(
                onPressed: () {
                  // Navigator.of(context).push(MaterialPageRoute(
                  //   builder: (context) => Loyalty(),
                  // ));
                  print("buton clicked");
                },
                color: Colors.teal[100],
                child: Image.asset('images/classmate.png')),
          ),
          Container(
            child: FlatButton(
                onPressed: () {
                  print("buton clicked");
                },
                color: Colors.teal[100],
                child: Image.asset('images/nippon.png')),
          ),
          Container(
            child: FlatButton(
                onPressed: () {
                  print("buton clicked");
                },
                color: Colors.teal[100],
                child: Image.asset('images/itc.png')),
          ),
          Container(
            child: FlatButton(
                onPressed: () {
                  print("buton clicked");
                },
                color: Colors.teal[100],
                child: Image.asset('images/cavinkare.png')),
          ),
          Container(
            child: FlatButton(
                onPressed: () {
                  print("buton clicked");
                },
                color: Colors.teal[100],
                child: Image.asset('images/star_health.png')),
          )
        ],
      ),
      // floatingActionButton: Container(
      //   height: 50.0,
      //   child: FloatingActionButton(
      //     onPressed: () {
      //       Navigator.of(context).push(MaterialPageRoute(
      //         builder: (context) => Contact(),
      //       ));
      //     },
      //     child: Icon(Icons.keyboard_arrow_right),
      //     backgroundColor: Colors.teal,
      //   ),
      // ),
      // floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    bottomNavigationBar: bottom(),
    
    );
  }
}
