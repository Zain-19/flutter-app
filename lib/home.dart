import 'package:ds_app/drawer.dart';
import 'package:flutter/material.dart';
import 'package:ds_app/bottom.dart';


class home extends StatefulWidget {
  @override
  _homeState createState() => _homeState();
}

class _homeState extends State<home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Mydrawer(),
      appBar: AppBar(
        backgroundColor: Colors.teal,
        title: Text(
          'Welcome to Deepsense...',
          style: TextStyle(
              fontFamily: 'Chilanka',
              fontSize: 24.0,
              fontWeight: FontWeight.bold,
              color: Colors.white),
        ),
        centerTitle: true,
      ),
      body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('images/background.png'), fit: BoxFit.cover)),
        child: SingleChildScrollView(
                  child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    'images/home_logo.JPG',
                    height: 220.0,
                  ),
                ],
              ),
              // SizedBox(
              //   height: 20.0,
              // ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15.0),
                child: Text(
                  'DeepSense Digital Solutions is engaged in providing the best digital solutions to all businesses.',
                  style: TextStyle(
                    fontFamily: 'Chilanka',
                    fontSize: 20.0,
                    color: Colors.teal,
                  ),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
                child: Text(
                  'With a motivated team of 50+ professionals driven by passion and having experience from 2-6 years in the digital industry, we cover the 360° of Digital Marketing.',
                  style: TextStyle(
                    fontFamily: 'Chilanka',
                    fontSize: 20.0,
                    color: Colors.teal,
                  ),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 15.0, vertical: 0.0),
                child: Text(
                  'We have a large and diversified clientele across various sectors ranging from Hospitality, Education, Healthcare, FMCG, and so on…',
                  style: TextStyle(
                    fontFamily: 'Chilanka',
                    fontSize: 20.0,
                    color: Colors.teal,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: bottom() ,
    );
  }
}
