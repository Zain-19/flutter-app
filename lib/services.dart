import 'package:ds_app/bottom.dart';
import 'package:ds_app/drawer.dart';
import 'package:flutter/material.dart';

class Services extends StatefulWidget {
  @override
  _ServicesState createState() => _ServicesState();
}

class _ServicesState extends State<Services> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Mydrawer(),
      appBar: AppBar(),
      body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('images/background.png'), fit: BoxFit.cover)),
        child: SingleChildScrollView(
                  child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                margin:
                    new EdgeInsets.symmetric(horizontal: 25.0, vertical: 15.0),
                color: Colors.teal[200],
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Text(
                    'Social Media Marketing',
                    style: TextStyle(
                        fontFamily: 'Sansita Swashed',
                        fontSize: 25.0,
                        color: Colors.white),
                  ),
                ),
              ),
              Container(
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                    children: [
                      Text(
                        '-> From Social Media post to \tCampaigns, we do it all!',
                        style: TextStyle(
                          fontSize: 25.0,
                          fontFamily: 'Chilanka',
                          wordSpacing: 3.0,
                        ),
                        textAlign: TextAlign.left,
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(
                        '-> We at DeepSense, guarantee active engagement on all major platforms like Facebook, Instagram, Twitter, YouTube, Pinterest and Blogs, creating a complete network of engaged audience.',
                        style: TextStyle(
                          fontSize: 25.0,
                          fontFamily: 'Chilanka',
                          wordSpacing: 3.0,
                        ),
                        textAlign: TextAlign.left,
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(
                        ' -> And the ship doesn’t stop there, we also ensure you achieve your sales objective.',
                        style: TextStyle(
                          fontSize: 25.0,
                          fontFamily: 'Chilanka',
                          wordSpacing: 2.0,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class Loyalty extends Services {
  var loyal = new Services();
}

class Contact extends StatelessWidget {
  // get bottomNavigationBar => BottomBar();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Mydrawer(),
      appBar: AppBar(
        title: Text(
          "Contact Us",
          style: TextStyle(fontFamily: 'Chilanka', fontSize: 30.0),
        ),
        centerTitle: true,
      ),
      body: Container(
          decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('images/contacts.png'),
          fit: BoxFit.cover,
        ),
      )),
      bottomNavigationBar: bottom(),

    );
  }
}
