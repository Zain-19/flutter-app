import 'package:ds_app/bottom.dart';
import 'package:ds_app/drawer.dart';
import 'package:flutter/material.dart';
import 'services.dart';

class grid extends StatefulWidget {
  @override
  _gridState createState() => _gridState();
}

class _gridState extends State<grid> {
  var ser = Services();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Mydrawer(),
      appBar: AppBar(
        backgroundColor: Colors.teal,
        title: Text(
          'Our Services',
          style: TextStyle(
              fontFamily: 'Chilanka',
              fontSize: 24.0,
              fontWeight: FontWeight.bold,
              color: Colors.white),
        ),
        centerTitle: true,
      ),
      body: GridView.count(
        crossAxisCount: 2,
        padding: EdgeInsets.all(5.0),
        mainAxisSpacing: 5.0,
        crossAxisSpacing: 5.0,
        children: [
          Container(
            child: FlatButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => Services(),
                ));
              },
              color: Colors.teal[100],
              child: Image.asset('images/media.jpg'),
            ),
          ),
          Container(
            child: FlatButton(
                onPressed: () {
                  // Navigator.of(context).push(MaterialPageRoute(
                  //   builder: (context) => Loyalty(),
                  // ));
                  print("buton clicked");
                },
                color: Colors.teal[100],
                child: Image.asset('images/loyalty.jpg')),
          ),
          Container(
            child: FlatButton(
                onPressed: () {
                  print("buton clicked");
                },
                color: Colors.teal[100],
                child: Image.asset('images/creative.jpg')),
          ),
          Container(
            child: FlatButton(
                onPressed: () {
                  print("buton clicked");
                },
                color: Colors.teal[100],
                child: Image.asset('images/seo.jpg')),
          ),
          Container(
            child: FlatButton(
                onPressed: () {
                  print("buton clicked");
                },
                color: Colors.teal[100],
                child: Image.asset('images/software.jpg')),
          ),
          Container(
            child: FlatButton(
                onPressed: () {
                  print("buton clicked");
                },
                color: Colors.teal[100],
                child: Image.asset('images/app.jpg')),
          )
        ],
      ),
      bottomNavigationBar: bottom(),
    );
  }
}
