import 'package:ds_app/clients.dart';
import 'package:ds_app/home.dart';
import 'package:ds_app/services.dart';
import 'package:flutter/material.dart';
import 'main.dart';
import 'grid.dart';

class Mydrawer extends StatefulWidget {
  @override
  _MydrawerState createState() => _MydrawerState();
}

class _MydrawerState extends State<Mydrawer> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            height: 30.0,
          ),
          Container(
            child: Padding(
              padding: const EdgeInsets.all(30.0),
              child: Column(
                children: [
                  Image.asset(
                    'images/logo.png',
                    height: 100.0,
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text(
                    'Deepsense.in',
                    style: TextStyle(fontSize: 20.0),
                  ),
                  Divider(height: 25.0, color: Colors.teal, thickness: 3.0),
                ],
              ),
            ),
            color: Colors.white10,
          ),
          Container(
            child: Column(
              children: [
                ListTile(
                    leading: Icon(Icons.home),
                    title: Text(
                      "Home",
                      style: TextStyle(fontSize: 20.0),
                    ),
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => home(),
                      ));
                    }),
                    ListTile(
                    leading: Icon(Icons.add_to_home_screen),
                    title: Text(
                      "Our Services",
                      style: TextStyle(fontSize: 20.0),
                    ),
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => grid(),
                      ));
                    }),
                ListTile(
                    leading: Icon(Icons.exit_to_app),
                    title: Text(
                      "Our Clients",
                      style: TextStyle(fontSize: 20.0),
                    ),
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => Client_grid(),
                      ));
                    }),
                ListTile(
                    leading: Icon(Icons.exit_to_app),
                    title: Text(
                      "Contact Us",
                      style: TextStyle(fontSize: 20.0),
                    ),
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => Contact(),
                      ));
                    }),
                ListTile(
                    leading: Icon(Icons.exit_to_app),
                    title: Text(
                      " Exit",
                      style: TextStyle(fontSize: 20.0),
                    ),
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => Mainpage(),
                      ));
                    }),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
