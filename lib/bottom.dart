import 'package:flutter/material.dart';
import 'home.dart';
import 'main.dart';
import 'grid.dart';
import 'clients.dart';
import 'services.dart';


class bottom extends StatefulWidget {
  @override
  _bottomState createState() => _bottomState();
}

class _bottomState extends State<bottom> {
  @override
  Widget build(BuildContext context) {
    return 
      BottomAppBar(
        color: Colors.teal,
        child: Row(
          children: [
            Container(
              width: 70.0,
              child: FlatButton(
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => home(),
                  ));
                },
                child: Icon(
                  Icons.home,
                  color: Colors.white,
                  size: 40.0,
                ),
              ),
            ),
            Container(
              width: 70.0,
              child: FlatButton(
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => grid(),
                  ));
                },
                child: Icon(
                  Icons.supervised_user_circle,
                  color: Colors.white,
                  size: 40.0,
                ),
              ),
            ),
            Container(
              width: 90.0,
              child: FlatButton(
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => Client_grid(),
                  ));
                },
                child: Icon(
                  Icons.supervisor_account,
                  color: Colors.white,
                  size: 40.0,
                ),
              ),
            ),
            Container(
              width: 90.0,
              child: FlatButton(
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => Contact(),
                  ));
                },
                child: Icon(
                  Icons.contact_mail,
                  color: Colors.white,
                  size: 40.0,
                ),
              ),
            ),
            Container(
              width: 70.0,
              child: FlatButton(
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => Mainpage(),
                  ));
                },
                child: Icon(
                  Icons.exit_to_app,
                  color: Colors.white,
                  size: 40.0,
                ),
              ),
            ),
          ],
        ),
      
      );
  
  }
}