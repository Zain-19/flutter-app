import 'package:flutter/material.dart';

class AnimatedContainerApp extends StatefulWidget {
  @override
  _AnimatedContainerAppState createState() => _AnimatedContainerAppState();
}

class _AnimatedContainerAppState extends State<AnimatedContainerApp> {
  var _color = Colors.red;
  var _height = 100.0;
  var _width = 100.0;
  animateContainer() {
    setState(() {
      _color = _color == Colors.red ? Colors.green : Colors.red;
      _height = _height == 100 ? 200 : 100;
      _width = _width == 100 ? 200 : 100;
    });
  }

  // Define the various properties with default values. Update these properties
  // when the user taps a FloatingActionButton.

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        
        appBar: AppBar(
          title: Text('Animated Container Demo'),
        ),
        body: 
        
        Column(
        
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            AnimatedContainer(
              duration: Duration(seconds: 1),
              color: _color,
              height: _height,
              width: _width,
            ),
            RaisedButton(
                child: Text('Click'),
                onPressed: () {
                  animateContainer();
                }),
          ],
        ),
      ),
    );
  }
}
